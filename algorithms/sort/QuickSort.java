package main.java.am.hsp.apps.algorithms.sort;

/**
 * Given array, which have n elements end print a sorted array with "Quick Sort"
 * algorithms.
 * 
 * @author lusine
 *
 */
public class QuickSort extends SortAlgorithms {

	public static void main(String[] args) {
		QuickSort alg = new QuickSort();
		alg.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		int[] array = { 12, 7, -2, 5, 6, 4, 1, -4 };

		System.out.println("Sorted array with Selection sort");

		printArray(sort(array));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.algorithms.sort.SortAlgorithms#sort(int[])
	 */
	@Override
	public int[] sort(int[] array) {
		getSotedArray(array, 0, array.length - 1);
		return array;
	}

	/**
	 * Return a sorted array with "Quick Sort" algorithms.
	 * 
	 * @param lowIndex
	 * @param highIndex
	 */
	private int[] getSotedArray(int[] array, int lowIndex, int highIndex) {
		int leftIndex = lowIndex;
		int rightIndex = highIndex;
		int pivot = array[getPivot(lowIndex, highIndex)];

		while (leftIndex <= rightIndex) {
			while (array[leftIndex] < pivot) {
				leftIndex++;
			}

			while (array[rightIndex] > pivot) {
				rightIndex--;
			}

			if (leftIndex <= rightIndex) {
				swapElements(array, leftIndex++, rightIndex--);

			}
		}
		
		if (lowIndex < rightIndex) {
			getSotedArray(array, lowIndex, rightIndex);
		}

		if (leftIndex < highIndex) {
			getSotedArray(array, leftIndex, highIndex);
		}

		return array;

	}

	/**
	 * Get a pivot index in the given array.
	 * 
	 * @param lowIndex
	 * @param highIndex
	 * @return
	 */
	private int getPivot(int lowIndex, int highIndex) {
		return lowIndex + (highIndex - lowIndex) / 2;
	}

}
