package main.java.am.hsp.apps.algorithms.sort;

/**
 * Given array, which have n elements end print a sorted array with "Bubble
 * Sort" algorithms.
 * 
 * @author lusine
 *
 */
public class BubbleSort extends SortAlgorithms {

    public static void main(String[] args) {
        BubbleSort alg = new BubbleSort();
        alg.solve();

    }

    /*
     * (non-Javadoc)
     * 
     * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
     */
    @Override
    public void solve() {
        int[] array = { 1, 5, 38, 7, -14, 2, -6, 15, -20, 21, 3 };

        System.out.println("Sorted array with Bubble sort");
        printArray(sort(array));

    }

    /*
     * (non-Javadoc)
     * 
     * @see main.java.am.hsp.apps.algorithms.sort.SortAlgorithms#sort(int[])
     */
    @Override
    public int[] sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {
                if (array[i] > array[j]) {
                    swapElements(array, i, j);
                }
            }
        }
        return array;
    }

 

}
