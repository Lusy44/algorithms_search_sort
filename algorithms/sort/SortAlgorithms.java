package main.java.am.hsp.apps.algorithms.sort;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;
/**
 * Base class for all the sorting algorithms. Will contain general methods.
 * @author lusine
 *
 */
public abstract class SortAlgorithms extends TaskArrayBase {
    
    /**
     * Return a sorted array
     * @param array
     * @return
     */
    public abstract int[] sort(int[] array);
    
    /**
     * Swap two elements in array
     * @param number1
     * @param number2
     */
    protected void swapElements(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

    /**
     * Return Min element of Array in given range
     * 
     * @param array
     * @return
     */
    protected int getMinIndexOfRange(int[] array, int beginIndex) {
        int min = array[beginIndex];
        int index = beginIndex;

        for (int i = beginIndex; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                index = i;
            }
        }

        return index;

    }
}
