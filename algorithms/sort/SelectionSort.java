package main.java.am.hsp.apps.algorithms.sort;

/**
 * Given array, which have n elements end print a sorted array with "Selection
 * Sort" algorithms.
 * 
 * @author lusine
 *
 */
public class SelectionSort extends SortAlgorithms {

    public static void main(String[] args) {
        SelectionSort alg = new SelectionSort();
        alg.solve();

    }

    /*
     * (non-Javadoc)
     * 
     * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
     */
    @Override
    public void solve() {
        int[] array = { 1, 5, -38, 7, -14, 2, 6, 15, 20, 21, 3 };

        System.out.println("Sorted array with Selection sort");
        printArray(sort(array));

    }

    /*
     * (non-Javadoc)
     * 
     * @see main.java.am.hsp.apps.algorithms.sort.SortAlgorithms#sort(int[])
     */
    @Override
    public int[] sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int index = getMinIndexOfRange(array, i);
            swapElements(array, i, index);

        }

        return array;
    }
}
