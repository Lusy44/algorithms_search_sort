package main.java.am.hsp.apps.algorithms.sort;

/**
 * Given array, which have n elements end print a sorted array with "Insertion
 * Sort" algorithms.
 * 
 * @author lusine
 *
 */
public class InsertionSort extends SortAlgorithms {

    public static void main(String[] args) {
        InsertionSort alg = new InsertionSort();
        alg.solve();

    }

    /*
     * (non-Javadoc)
     * 
     * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
     */
    @Override
    public void solve() {
        int[] array = { -1, 5, 0, 38, 7, -14, 2, 6, 15, 20, 21, 3 };

        System.out.println("Sorted array with Insertion sort");
        printArray(sort(array));

    }

    /*
     * (non-Javadoc)
     * 
     * @see main.java.am.hsp.apps.algorithms.sort.SortAlgorithms#sort(int[])
     */
    @Override
    public int[] sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int j = i; j > 0; j--) {
                if (array[j] < array[j - 1]) {
                    swapElements(array, j, j - 1);
                }
            }

        }
        
        return array;
    }

}
