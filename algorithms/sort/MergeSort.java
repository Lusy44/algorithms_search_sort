package main.java.am.hsp.apps.algorithms.sort;

/**
 * Given array, which have n elements end print a sorted array with "Merge Sort"
 * algorithms.
 * 
 * @author lusine
 *
 */
public class MergeSort extends SortAlgorithms {
	private int[] array = { 1, 5, 38, 7, -14, 2, -6, 9, 6, 15, 20, 21, 3 };

	public static void main(String[] args) {
		MergeSort alg = new MergeSort();
		alg.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		System.out.println("Sorted array with Merge Sort");
		printArray(sort(array));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.algorithms.sort.SortAlgorithms#sort(int[])
	 */
	@Override
	public int[] sort(int[] array) {
		doMergeSotd(0, array.length - 1);
		return array;
	}

	/**
	 * Sorts the array
	 * 
	 * @param beginIndex
	 * @param endIndex
	 */
	private void doMergeSotd(int beginIndex, int endIndex) {
		if (beginIndex < endIndex) {
			int middle = getMiddleIndex(beginIndex, endIndex);

			doMergeSotd(beginIndex, middle);
			doMergeSotd(middle + 1, endIndex);
			mergeParts(beginIndex, middle, endIndex);
		}
	}

	/**
	 * Get middle index in the given array.
	 * 
	 * @param lowIndex
	 * @param highIndex
	 * @return
	 */
	private int getMiddleIndex(int lowIndex, int highIndex) {
		return lowIndex + (highIndex - lowIndex) / 2;
	}

	/**
	 * Merges the Sorted parts
	 */
	private void mergeParts(int lowIndex, int midIndex, int highIndex) {
		int[] arrayCopy = copyArray(lowIndex, highIndex);
		
		int leftIndex = lowIndex;
		int rightIndex = midIndex + 1;
		int sortIndex = leftIndex;

		while (leftIndex <= midIndex && rightIndex <= highIndex) {
			if (arrayCopy[leftIndex] <= arrayCopy[rightIndex]) {
				array[sortIndex++] = arrayCopy[leftIndex++];
			} else {
				array[sortIndex++] = arrayCopy[rightIndex++];
			}
		}

		while (leftIndex <= midIndex) {
			array[sortIndex++] = arrayCopy[leftIndex++];
		}
	}

	/**
	 * copy given array
	 * @param lowIndex
	 * @param highIndex
	 * @return
	 */
	private int[] copyArray(int lowIndex, int highIndex) {
		int[] arrayCopy = new int[array.length];
		
		for (int i = lowIndex; i <= highIndex; i++) {
			arrayCopy[i] = array[i];
		}
		
		return arrayCopy;
	}

}
