package main.java.am.hsp.apps.algorithms.search;

/**
 * Given a sorted array of n elements... Print the index of element in array
 * with "Interpolation Search" method, which value equal a given number in
 * array.
 * 
 * @author lusine
 *
 */
public class InterpolationSearch extends BinAndInterSearchBase {

	public static void main(String[] args) {
		InterpolationSearch alg = new InterpolationSearch();
		alg.solve();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		number = getUserInput("Please, enter number");

		int search = search();
		System.out.println((search == -1) ? "Element not present" : "Element found at index " + search);

		int searchByAlgorithmWithRecursion = searchByAlgorithmWithRecursion(0, array.length - 1);
		
		System.out.println((searchByAlgorithmWithRecursion == -1) ? "Element not present with recursion"
				: "Diside index with recursion = " + searchByAlgorithmWithRecursion);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.algorithms.search.SearchInSortedArrayAlgorithm#
	 * searchByAlgorithm()
	 */
	@Override
	protected int searchByAlgorithm() {
		int beginIndex = 0;
		int endIndex = array.length - 1;

		while (beginIndex < endIndex) {
			if (isValidToContinue(beginIndex, endIndex)) {
				return NOT_SOLUTION;
			}

			int pivot = getPivotIndex(beginIndex, endIndex);

			if (array[pivot] == number) {
				return pivot;
			}

			if (array[pivot] < number) {
				beginIndex = pivot + 1;
			} else {
				endIndex = pivot - 1;
			}
		}

		return NOT_SOLUTION;
	}

	/**
	 * Check is Valid To Continue
	 * 
	 * @param array
	 * @param number
	 * @return
	 */
	private boolean isValidToContinue(int beginIndex, int endIndex) {
		return (isNumberOutOfRange(number, array[beginIndex], array[endIndex]) && array[beginIndex] != array[endIndex]);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * main.java.am.hsp.apps.algorithms.search.BinAndInterSearchBase#getPivotIndex(
	 * int, int)
	 */
	@Override
	protected int getPivotIndex(int beginIndex, int endIndex) {
		return beginIndex
				+ ((number - array[beginIndex])) * (endIndex - beginIndex) / (array[endIndex] - array[beginIndex]);
	}

}