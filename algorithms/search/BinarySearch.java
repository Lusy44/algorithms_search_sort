package main.java.am.hsp.apps.algorithms.search;

/**
 * Given a sorted array of n elements... Print the index of "Binary Search"
 * method, which value equal a given number in array.
 * 
 * @author lusine
 *
 */
public class BinarySearch extends BinAndInterSearchBase {

	public static void main(String[] args) {
		BinarySearch alg = new BinarySearch();
		alg.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		number = getUserInput("Please, enter number");

		int search = search();
		System.out.println((search == -1) ? "Element not present" : "Element found at index " + search);

		int searchByAlgorithmWithRecursion = searchByAlgorithmWithRecursion(0, array.length - 1);

		System.out.println((searchByAlgorithmWithRecursion == -1) ? "Element not present with recursion"
				: "Diside index with recursion = " + searchByAlgorithmWithRecursion);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.algorithms.search.SearchInSortedArrayAlgorithm#
	 * searchByAlgorithm()
	 */
	@Override
	protected int searchByAlgorithm() {
		int beginIndex = 0;
		int endIndex = array.length - 1;

		while (beginIndex <= endIndex) {
			int pivot = getPivotIndex(beginIndex, endIndex);

			if (array[pivot] == number) {
				return pivot;
			}
			
			if (array[pivot] > number) {
				endIndex = pivot - 1;
			} else {
				beginIndex = pivot + 1;

			}

		}

		return NOT_SOLUTION;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * main.java.am.hsp.apps.algorithms.search.BinAndInterSearchBase#getPivotIndex(
	 * int, int)
	 */
	@Override
	protected int getPivotIndex(int beginIndex, int endIndex) {
		return beginIndex + (endIndex - beginIndex) / 2;
	}

}