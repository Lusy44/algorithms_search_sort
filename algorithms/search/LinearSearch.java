package main.java.am.hsp.apps.algorithms.search;

/**
 * Given array of n elements... Print the index of element in array with "Linear
 * Search" method, which value equal a given number in array.
 * 
 * @author lusine
 *
 */
public class LinearSearch extends SearchAlgorithms {
	public int array[] = { 10, 40, 25, 30, -50, 25, 110, 120, 130, 170 };

	public static void main(String[] args) {
		LinearSearch alg = new LinearSearch();
		alg.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		number = getUserInput("Please, enter number");

		int search = search();
		System.out.println((search == -1) ? "Element not present" : "Element found at index " + search);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.algorithms.search.SearchAlgorithms#search(int[],
	 * int)
	 */
	@Override
	public int search() {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == number) {
				return i;
			}
		}

		return NOT_SOLUTION;
	}

	/**
	 * Return index of element from given range, which equal the given number
	 * 
	 * @param array
	 * @param beginIndex
	 * @param endIndex
	 * @param number
	 * @return
	 */
	public int linearSearchGivenRange(int[] array, int number, int beginIndex, int endIndex) {
		for (int i = endIndex; i > beginIndex; i--) {
			if (array[i] == number) {
				return i;
			}
		}

		return NOT_SOLUTION;
	}
}