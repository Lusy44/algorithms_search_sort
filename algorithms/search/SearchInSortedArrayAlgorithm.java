package main.java.am.hsp.apps.algorithms.search;

/**
 * Base class for all the sorted arrays in search algorithms. Will contain
 * general methods.
 * 
 * @author lusine
 *
 */
public abstract class SearchInSortedArrayAlgorithm extends SearchAlgorithms {
    protected int array[] = { 10, 20, 25, 30, 50, 60, 65, 110, 120, 130, 150, 160, 165, 168, 170, 190 };

    /*
     * (non-Javadoc)
     * 
     * @see main.java.am.hsp.apps.algorithms.search.SearchAlgorithms#search()
     */
    public final int search() {
        if (isNumberOutOfRange(number, array[0], array[array.length - 1])) {
            return NOT_SOLUTION;
        }

        return searchByAlgorithm();

    }

    /**
     * Abstract method for each tasks.
     * 
     * @param array
     * @param number
     * @return
     */
    protected abstract int searchByAlgorithm();

    /**
     * Check number is in range
     * 
     * @param number
     * @param begin
     * @param end
     * @return
     */
    protected boolean isNumberOutOfRange(int number, int beginElm, int endElm) {
        return (beginElm > number || number > endElm);
    }

}
