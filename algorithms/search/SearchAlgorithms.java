package main.java.am.hsp.apps.algorithms.search;

import main.java.am.hsp.apps.seua.base.TaskArrayBase;

/**
 * Base class for all the search algorithms. Will contain general methods.
 * 
 * @author lusine
 *
 */
public abstract class SearchAlgorithms extends TaskArrayBase {
    protected static final int NOT_SOLUTION = -1;
    protected int number;

    /**
     * Get index of element in array, which equal number
     * 
     * @param array
     * @param number
     * @return
     */
    public abstract int search();

    /**
     * Create object of the Linear search class and call "linearSearchGivenRange" method
     * 
     * @param array
     * @param beginIndex
     * @param endIndex
     * @param number
     * @return
     */
    protected int getIndexByLinearSearch(int[] array, int number, int beginIndex, int endIndex) {
        LinearSearch alg = new LinearSearch();
        return alg.linearSearchGivenRange(array, number, beginIndex, endIndex);
    }

}