package main.java.am.hsp.apps.algorithms.search;

/**
 * Base class for Binary and Interpolation search algorithms(recursion method)
 * 
 * @author lusine
 *
 */
public abstract class BinAndInterSearchBase extends SearchInSortedArrayAlgorithm {

    /**
     * Return index element the given number from array, otherwise Return NOT
     * SOLUTION.
     * 
     * @param beginIndex
     * @param endIndex
     * @return
     */
    protected final int searchByAlgorithmWithRecursion(int beginIndex, int endIndex) {
        if (isNumberOutOfRange(number, array[beginIndex], array[endIndex])) {
            return NOT_SOLUTION;
        }

        int pivot = getPivotIndex(beginIndex, endIndex);

        if (array[pivot] == number) {
            return pivot;
        }
        if (array[pivot] > number) {
            return searchByAlgorithmWithRecursion(beginIndex, pivot - 1);
        }
        
        return searchByAlgorithmWithRecursion(pivot + 1, endIndex);

    }

    /**
     * Return Pivot Index
     */
    protected abstract int getPivotIndex(int beginIndex, int endIndex);

}
