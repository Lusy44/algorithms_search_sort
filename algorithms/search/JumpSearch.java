package main.java.am.hsp.apps.algorithms.search;

/**
 * Given a sorted array of n elements... Print the index of element in array
 * with "Jump Search" method, which value equal a given number in array.
 * 
 * @author lusine
 *
 */
public class JumpSearch extends SearchInSortedArrayAlgorithm {

	public static void main(String[] args) {
		JumpSearch alg = new JumpSearch();
		alg.solve();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.algorithms.search.SearchInSortedArrayAlgorithm#
	 * searchByAlgorithm()
	 */
	@Override
	protected int searchByAlgorithm() {
		int step = getStep(array);
		int index = 0;

		while (array[index] < number) {
			index += step;
			if (index >= array.length - 1) {
				index = array.length - 1;
			}
		}

		if (array[index] == number) {
			return index;
		}

		return getIndexByLinearSearch(array, number, index - step, index - 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see main.java.am.hsp.apps.seua.base.TaskBase#solve()
	 */
	@Override
	public void solve() {
		number = getUserInput("Please, enter number");

		int search = search();
		System.out.println((search == -1) ? "Element not present" : "Element found at index " + search);
	}

	/**
	 * Return step from the given array
	 * 
	 * @param beginRange
	 * @param endRange
	 * @return
	 */
	private int getStep(int[] array) {
		return (int) Math.sqrt(array.length);
	}

}